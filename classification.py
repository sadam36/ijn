#!/usr/bin/python2
#-*- coding: utf-8 -*-

import sys, os
import subprocess
import argparse 
from createModel import preprocessCCL, readStopList, xmlPattern, txtPattern

from os.path import basename
from sklearn.metrics import confusion_matrix
from sklearn.metrics import f1_score
from sklearn.metrics import classification_report


logprobConst = "logprob="
pplConst = "ppl="
ppl1Const = "ppl1="

usingPPLConst = ppl1Const


corpuses = []
corpusesBaseNames = []

files = {}


initialDirectory = os.getcwd()

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument("documents", help="File with document paths to classify")
	parser.add_argument("corpuses", help="File with corpuses paths to test")
	parser.add_argument("--stoplist", help="Stop list")
	parser.add_argument("--output", help="Output file")
	#parser.add_argument("--txt", action='store_true', help="Read .txt files")
	parser.add_argument("--ppl", action='store_true', help="Use ppl measure instead of ppl1")
	args = parser.parse_args()
	
	stoplist = readStopList(args.stoplist)
	readFilesPaths(args.documents)
	readCorpusesPaths(args.corpuses)
	
	if args.ppl:
		global usingPPLConst
		usingPPLConst = pplConst

	supposed, pred = checkForEachFile(stoplist)

	outputFile = args.output
	printClassificationStats(supposed, pred, outputFile)


'''Wczytanie sciezek do plikow z korpusami z pliku'''
def readCorpusesPaths(path):
	os.chdir(initialDirectory)
	os.chdir(os.path.dirname(path))

	for line in open(os.path.basename(path)):
		corpuses.append(os.path.abspath(line.strip()))
		corpusesBaseNames.append(basename(line.strip()))

'''Wczytanie sciezek plikow testowych z podanego pliku (z )'''
def readFilesPaths(path):
	os.chdir(os.path.dirname(path))

	for line in open(os.path.basename(path)):
		splitted = line.split()
		files[os.path.abspath(splitted[0])] = splitted[1]

'''Dla kazdego pliku testowego odpala zapisanie do .txt i odpalenie '''
def checkForEachFile(stoplist):
	x_supposed = []
	y_pred = []

	for file in files:
		pattern = xmlPattern
		replacePattern = txtPattern

		if file.endswith(txtPattern):
			pattern = txtPattern
			replacePattern = "_text" + txtPattern

		txtDocument = file.replace(pattern, replacePattern)

		preprocessCCL(file, txtDocument, stoplist)

		supposedResult = files[file]
		x_supposed.append(corpusesBaseNames.index(supposedResult))

		y_pred.append(corpusesBaseNames.index(findBestMatch(txtDocument, supposedResult)))
	
	return x_supposed, y_pred


'''Sprawdza prawdopodobienstwo dla wszystkich korpusow z listy i wybiera najlepsze korzystajac z perplexity'''
def findBestMatch(document, supposedResult):
	results = {}
	for corpus in corpuses:
		output = evaluate(corpus, document)
		
		results[basename(corpus)] = float(getPerplexityFromNgramOutput(corpus, output))
	
	print "Found: ", findBestInMap(results).strip(), " with perplexity=", results[findBestInMap(results)], "supposed result: ", supposedResult
	
	return findBestInMap(results).strip()


'''Znajduje najlepszy wynik w mapie '''
def findBestInMap(result):
	return min(result, key=result.get)

'''Ewaluacja dla modelu i korpusu (korzysta z ngram)'''
def evaluate(corpus, document):
	callArgs = ["ngram", "-lm", corpus.strip(), "-ppl", document.strip()]
	return subprocess.check_output(callArgs)


'''Statystyki takie jak perplexity itd dostajemy w formie stringa, trzeba wydobyc to co tam jest i zapisac co wyszlo dla podanego korpusu'''
def getPerplexityFromNgramOutput(corpus, output):
	splittedStats = output.split()
	print splittedStats
	ppl = splittedStats[splittedStats.index(usingPPLConst) + 1]
	print ppl
	return ppl

def printClassificationStats(supposed, predictions, outputFile):
	os.chdir(initialDirectory)
	print supposed
	print predictions
	output = "Confusion matrix: \n"
	output += str(confusion_matrix(supposed, predictions)) + "\n"
	output += "F1 score: " + str(f1_score(supposed, predictions)) + "\n"
	output += classification_report(supposed, predictions, target_names=corpusesBaseNames) + "\n"

	print output

	if outputFile:
		f = open(outputFile, "w+")
		f.write(output)
		f.close()


if __name__ == '__main__':
	main()
