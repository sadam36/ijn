#!/usr/bin/python2
#-*- coding: utf-8 -*-


from lxml import etree
import re
import os

try:
	import tagger
except:
	print "Tagger unavaliable"

class CCLParser(object):

	def __init__(self, fileIn, fileOut, stopList):
		self.fileIn = fileIn
		self.fileOut = fileOut
		self.stopList = stopList
		self.digitRegexp = re.compile('\d')

		if fileIn.endswith('.txt'):
			self.xmlOutputPath = self.fileIn[0:len(fileIn) - 4] + ".xml"
			configPath = os.environ.get('WCRFT_CONFIG')
			
			if not configPath:
				configPath = '/home/nlp/apps/wcrft/config/nkjp.ini'

			data_dir = os.environ.get('WCRFT_DATA')
			
			if not data_dir:
				data_dir = '/home/nlp/apps/model_nkjp10_wcrft/'

			tagr = tagger.Tagger(configPath, data_dir)
			tagr.load_model()
			tagr.tag_input(self.fileIn, self.xmlOutputPath, 'txt', 'ccl', False, False)
			self.fileIn = self.xmlOutputPath

	'''Parsuje CCLa, odrzuca rzeczy ze stoplisty i zapisuje do pliku lematy'''
	def parse(self):
		result = []

		tree = etree.parse(self.fileIn)

		root = tree.getroot() 

		tokens = [ b for b in root.iterfind(".//tok") ]

		for token in tokens:
			lexems = [l for l in token.iterfind(".//lex")]
			firstBase = lexems[0].find(".//base").text #TODO tutaj powinno byc to co jest disamb, ale w kpwr nie widze takiego nigdzie :|
			if not firstBase.encode('utf-8') in self.stopList and not self.contains_digits(firstBase):
				if firstBase == '.':
					result.append('\n')
				else:
					result.append(firstBase.lower())

		f = open(self.fileOut, 'w')
		tmp = u' '.join(result)
		f.write(tmp.encode('utf8'))
		f.close()

	def contains_digits(self, d):
		return bool(self.digitRegexp.search(d))