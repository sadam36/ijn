#!/usr/bin/python3

import sys, os
import subprocess
import glob
import argparse
from cclParser import CCLParser

xmlPattern = ".xml"
txtPattern = ".txt"
countPattern = ".count"
relXmlPattern = ".rel.xml"

allNgramsFilename="allNgrams"

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument("directory", help="Directory from which we create model")
	parser.add_argument("output", help="Output of created language model")
	parser.add_argument("--stoplist", help="Stop list file")
	args = parser.parse_args()
	
    print(args)
    
	runNgramCountForDirectory(args)
	buildModel(args.output)

'''Dla wszystkich plikow txt (wczesniej wyciagnietych po preprocessingu z xmli) z folderu odpala zliczanie ngramow'''
def runNgramCountForDirectory(args):
	stoplist = readStopList(args.stoplist)

	os.chdir(args.directory)
	relFiles = glob.glob("*" + relXmlPattern)
	for file in glob.glob("*" + xmlPattern):
		if file not in relFiles:
			txtFile = file.replace(xmlPattern, txtPattern)
			countFile = file.replace(xmlPattern, countPattern)
		
			preprocessCCL(file, txtFile, stoplist)
            
'''Zwraca liste slow wczytana z pliku stoplisty'''
def readStopList(stopListFile):
	stopWords = []
	if stopListFile != None:
		for word in open(stopListFile):
			stopWords.append(word.strip())
#	print ("stopWords=", str(stopWords))
	return stopWords
    
'''Odpala preprocessing dla CCLa (fileIn) - parsuje XML, odrzuca rzeczy ze stoplisty i zapisuje lematy do pliku'''
def preprocessCCL(fileIn, fileOut, stoplist):
	cclParser = CCLParser(fileIn, fileOut, stoplist)
	cclParser.parse()
