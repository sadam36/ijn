#!/usr/bin/python2
#-*- coding: utf-8 -*-

import sys, os
import subprocess
import glob
import argparse
from cclParser import CCLParser

xmlPattern = ".xml"
txtPattern = ".txt"
countPattern = ".count"
relXmlPattern = ".rel.xml"

allNgramsFilename="allNgrams"

possibleDiscounting = ["c", "kn", "gt", "ukn", "wb", "n",  "add"]
discountMappingToSrilmParam = {"c" : "cdiscount", "n" : "ndiscount", "wb" : "wbdiscount", "kn": "kndiscount", "ukn" : "ukndiscount", "add" : "addsmooth"} #"count" :  "count-lm",

initialDirectory = os.getcwd()

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument("directory", help="Directory from which we create model")
	parser.add_argument("output", help="Output of created language model")
	parser.add_argument("-d", "--discounting", help="Discouting. Possible values: c (Ney\'s absolute), kn (Kneser-Ney modified), ukn (Kneser-Ney unmodified), wb (Witten-Bell), n (Ristad's natural discouting law), add (Additional smoothing). Default: Good Turing aka Katz")
	parser.add_argument("--discountingOrder", type=int, help="Discouting order. Default: 3")
	parser.add_argument("--interpolation", action="store_true", help="Interpolated version of discounting (instead of backoff). Doesn\'t work for ndiscount and Katz")
	parser.add_argument("-o", "--order", type=int, help="Maxiumum order of ngrams. Default: 3")
	parser.add_argument("--delta", help="Constant for addictive (should be > 0) or cdiscount smoothing (should be <0,1>). Default 1.")
	parser.add_argument("--stoplist", help="Stop list file")
	parser.add_argument("-a", "--additional", help="Additional arguments passed to ngram-count.")
	parser.add_argument("--txt", action='store_true', help="Read .txt files")
	args = parser.parse_args()
	
	runNgramCountForDirectory(args)
	buildModel(args.output, args)
	


'''Dla wszystkich plikow txt (wczesniej wyciagnietych po preprocessingu z xmli) z folderu odpala zliczanie ngramow'''
def runNgramCountForDirectory(args):
	stoplist = readStopList(args.stoplist)

	os.chdir(args.directory)
	relFiles = glob.glob("*" + relXmlPattern)

	pattern = xmlPattern
	replacePattern = txtPattern
	if args.txt:
		pattern = txtPattern
		replacePattern = "_tmp_" + txtPattern

	for file in glob.glob("*" + pattern):
		if file not in relFiles:
			txtFile = file.replace(pattern, replacePattern)
			countFile = file.replace(pattern, countPattern)
		
			preprocessCCL(file, txtFile, stoplist)
			runNgramCountForFile(txtFile, countFile, args)

'''Zwraca liste slow wczytana z pliku stoplisty'''
def readStopList(stopListFile):
	stopWords = []
	if stopListFile != None:
		for word in open(stopListFile):
			stopWords.append(word.strip())
	return stopWords


'''Odpala preprocessing dla CCLa (fileIn) - parsuje XML, odrzuca rzeczy ze stoplisty i zapisuje lematy do pliku'''
def preprocessCCL(fileIn, fileOut, stoplist):
	cclParser = CCLParser(fileIn, fileOut, stoplist)
	cclParser.parse()


'''Odpala ngram-count z parametrami dla pliku .txt'''
def runNgramCountForFile(fileIn, fileOut, args):
	callArgs = ["ngram-count", "-text", fileIn, "-write", fileOut]
	callArgs = parseParamsForNgramCount(callArgs, args)

	print callArgs
	print os.getcwd()
	subprocess.call(callArgs)

#	if ret:
#		print("Error ", ret)
#		sys.exit()


'''Z linii komend zwraca liste parametrow do ngram-count'''
def parseParamsForNgramCount(callArgs, args):
	discount = args.discounting
	discountOrder = args.discountingOrder
	interpolation = args.interpolation

	delta = args.delta

	if not delta or float(delta) < 0:
		delta = 1

	callArgs = addDiscountParams(callArgs, discount, interpolation, discountOrder, delta)
	return callArgs

'''Na podstawie stringa dorzuca do argumentow parametry discount'''
def addDiscountParams(callArgs, discounting, interpolation, discountingOrder, delta):
	
	if not discounting or discounting == 'gt':
		return callArgs

	if not discounting in possibleDiscounting:
		print "Unknown discounting"
		sys.exit()
	
	if interpolation:
		callArgs.append("-interpolate")
	
	if not discountingOrder or discountingOrder < 0:
		discountingOrder = 3

	callArgs.append("-" + discountMappingToSrilmParam[discounting]+ str(discountingOrder))

	if discounting == 'c' or discounting == 'add':
		callArgs.append(str(delta))
		
	return callArgs

'''Buduje model przez polaczenie wszystkich countow (uzywa ngram-count)'''
def buildModel(output, args):
	mergeNgrams(allNgramsFilename, args)

	ngramsFile = os.getcwd() + os.sep + allNgramsFilename
	os.chdir(initialDirectory)
	
	callArgs = ["ngram-count", "-read", ngramsFile, "-lm", output]
	callArgs = parseParamsForNgramCount(callArgs, args)
	subprocess.check_output(callArgs)

'''Merguje ngramy do jednego pliku (uzywa ngram-merge)'''
def mergeNgrams(filename, args):
	callArgs = ["ngram-merge"]
	callArgs.extend(glob.glob("*" + countPattern))
	
	output = subprocess.check_output(callArgs)
	output = output.decode('utf8')
	f = open(filename, "w")
	f.write(output.encode('utf8'))
	f.close()


if __name__ == '__main__':
	main()
